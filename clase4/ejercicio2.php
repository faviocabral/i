<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="descripcion" content="Mi Primer Script PHP">
<title>Mi Primer Script PHP</title>
<style>

/*alinear la tabla , poner lineas y bordes */
table, td, th {
  border: 1px solid black;
  border-collapse: collapse;
  margin-left:auto;
  margin-right:auto;
}

/* las 2 ultimas columnas textos alineados al centro */
table td:nth-child(2),td:nth-child(3){
	text-align:center;
}

/*cabecera principal*/
table tr:nth-child(1){
	background-color:yellow;
} 

/*cabecera secundaria*/
table tr:nth-child(2){
	background-color:silver;
}

/*intercalar color verde agua entre columnas */
tr:nth-child(even) {background-color: #e2efff;}
 
 /*definir ancho de la tabla*/
table {
	width:30%;
} 


</style>
</head>
<body>

<?php
	echo '
<table >
  <tr>
    <th colspan="3" >Productos</th>
  </tr>
  <tr>
    <th>Nombre</th>
    <th>Cantidad</th>
    <th>Precio (Gs)</th>
  </tr>
  <tr>
    <td>Coca Cola</td>
    <td>100</td>
    <td>4.500</td>
  </tr>
  <tr>
    <td>Pepsi Cola</td>
    <td>30</td>
    <td>4.800</td>
  </tr>
  <tr>
    <td>Sprite</td>
    <td>20</td>
    <td>4.500</td>
  </tr>
  <tr>
    <td>Guarana</td>
    <td>200</td>
    <td>4.500</td>
  </tr>
  <tr>
    <td>SevenUP</td>
    <td>24</td>
    <td>4.800</td>
  </tr>
  <tr>
    <td>Mirinda Naranja</td>
    <td>56</td>
    <td>4.800</td>
  </tr>
  <tr>
    <td>Mirinda Guarana</td>
    <td>89</td>
    <td>4.800</td>
  </tr>
  <tr>
    <td>Fanta Naranja</td>
    <td>10</td>
    <td>4.500</td>
  </tr>
  <tr>
    <td>Fanta Piña</td>
    <td>2</td>
    <td>4.500</td>
  </tr>
  
</table>	
	';
	//"Hola Mundo !!!!";
?>

</body>
</html>
