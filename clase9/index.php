<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-expand-sm bg-light fixed">
  <ul class="navbar-nav">
  <li class="nav-item">
      <a class="btn btn-success" href="#"><strong>Nuevo Registro</strong></a>
    </li>
    <li class="nav-item ml-3">
      <a class="btn btn-danger" href="#" onclick="logout()"><strong>Cerrar Session</strong></a>
    </li>
    <li class="nav-item ml-3">
      <a class="nav-link" href="#" id= "user"></a>
    </li>
  </ul>
</nav>
<br>

<div class="container">
  <h3>Listado de producto</h3>
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>Producto</th>
        <th>Descripcion</th>
        <th>Marca</th>
        <th>Tipo</th>
        <th></th>
      </tr>
    </thead>
    <tbody id="resultados">
    </tbody>
  </table>

</div>

<script type="text/javascript">
if(!localStorage.getItem('login')){
    location.replace("http://localhost/facu/ejercicio5/login.php");
}
$("#user").html('<strong>Bienvenido '+localStorage.getItem('usuario')+ ' !!!</strong>');
function listar () {
    $.ajax({
            method: "GET",
            url: "accion.php",
            dataType: 'json'
            ,data: { accion: "listar" }
    })
    .done(function( rs ) {
        console.log("datos correctos");
        console.log(rs);
        var resultado = "";
        rs.forEach( function ( rs2 ){ //fco recorre la lista de resultados por cada  objeto[](campos[])
            resultado += "<tr>";
            resultado += "<td>" + rs2['codigo'] + "</td>"; 
            resultado += "<td>" + rs2['producto'] + "</td>"; 
            resultado += "<td>" + rs2['descripcion'] + "</td>"; 
            resultado += "<td>" + rs2['tipo'] + "</td>"; 
            resultado += "<td>" + rs2['marca'] + "</td>"; 
            resultado += '<td><div class="btn-group">'+
                            '<button type="button" class="btn btn-warning" onclick="editar('+rs2['codigo']+')">Editar</button>'+
                            '<button type="button" class="btn btn-danger" onclick="borrar('+rs2['codigo']+')">Borrar</button>'+
                            '</div></td>'; 
            resultado += '</tr>';
        });
                        resultado += "<tr>";
 $("#resultados").html(resultado);
    })
    .fail(function() {
        alert( "error" );
    })

}
listar();

function editar(id){
    alert(id);
}
function borrar(id){
    alert(id);
}

function logout(){
    localStorage.clear();
    location.reload();
}
</script>
</body>
</html>
